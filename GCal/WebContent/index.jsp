<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>GCal - Google Calendar Analyser</title>
  	<link rel="stylesheet" href="css/app.css"/>
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/bootstrap-responsive.min.css"/>
	<link rel="stylesheet" href="css/cupertino/jquery-ui-1.8.21.custom.css"/>
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.21.custom.min.js"></script>
</head>
<body>

<%@ include file="modules/setparameters.jsp" %>
<%@ include file="modules/showsearchresult.jsp" %>
<%@ include file="modules/showcalendars.jsp" %>


</body>
</html>