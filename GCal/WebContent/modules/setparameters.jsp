
<%@page import="com.google.gdata.data.DateTime"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="it.bz.tis.gcal.GCalContext"%>
<%@page import="google.gdata.DateTime"%>
<div id="selectstocksymbol">
<%
{	// start own name space
	DateFormat df= new SimpleDateFormat("dd/MM/yyyy");
	String username = GCalContext.getInstance(request).getUsername();
	String password = GCalContext.getInstance(request).getPassword();
	String searchString = GCalContext.getInstance(request).getSearchString();
	String startdate,enddate;
	DateTime googleSDate=GCalContext.getInstance(request).getStartDate();
	DateTime googleEDate=GCalContext.getInstance(request).getEndDate();
	if (googleSDate!=null)
		startdate = df.format(new Date(googleSDate.getValue()));
	if (googleEDate!=null)
		enddate = df.format(new Date(googleEDate.getValue()));
%>
<div class="container">
	<form action="SetParameters" method="post" class="form-horizontal" >
	 <fieldset>
	    <legend>Calendar Form</legend>
	    <div class="control-group">
	      <label class="control-label" for="username">Username</label>
	      <div class="controls">
	       <input type="text" name="username" required="required" id="username" size="30" maxlength="30" autocomplete="on"
			<%if (username != null) { %> 
	      		value="<%=username %>"
			<%} %> 
	      	/>
	        <p class="help-block">Insert gmail account</p>
	      </div>
	    </div>
	    <div class="control-group">
	      <label class="control-label" for="password">Password</label>
	      <div class="controls">
	      <input type="password" required="required" name="password" id="password" size="30" maxlength="30" autocomplete="on"
			<%if (password != null) { %> 
	      		value="<%=password %>"
			<%} %> 
	      	/>
	        <p class="help-block">Insert password</p>
	      </div>
	    </div>
		<div class="control-group">
	      <label class="control-label" for="searchstring">Search String</label>
	      <div class="controls">
	      <input type="text" name="searchstring" id="searchstring" size="50" maxlength="50"
			<%if (searchString != null) { %> 
	      		value="<%=searchString %>"
			<%} %> 
	      	/>
	        <p class="help-block">Insert project you're searching for</p>
	      </div>
	    </div>
		<div class="control-group">
	      <label class="control-label" for="searchstring">Timespan</label>
	      <div class="controls">
	      	<p class="help-block">Insert from</p>
	      	<input type="text" name="startdate" id="startdate" size="50" maxlength="50" 
	      	<%if (startdate != null) { %> 
	      		value="<%=startdate %>"
			<%} %> />
	        <p class="help-block">Until</p>
	        <input type="text" name="enddate" id="enddate" size="50" maxlength="50"
	        <%if (enddate != null) { %> 
	      		value="<%=enddate %>"
			<%} %> 
	        />
	      
	      </div>
	    </div>
	    <script>
		$(function() {
			$( "#startdate" ).datepicker({
					showWeek: true,
					firstDay: 1,
					numberOfMonths: 3,
					showButtonPanel: true,
					dateFormat:"dd/mm/yy"
			});
			$( "#enddate" ).datepicker({
				showWeek: true,
				firstDay: 1,
				numberOfMonths: 3,
				showButtonPanel: true,
				dateFormat:"dd/mm/yy"
		});
		});
		</script>
		<div class="form-actions">
	            <button type="submit" class="btn btn-primary">Submit</button>	       
	     </div>
	 </fieldset>
	</form>
</div>
<%} %>
</div>
