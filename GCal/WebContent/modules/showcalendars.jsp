
<%@page import="com.google.gdata.data.calendar.CalendarFeed"%>
<%@page import="it.bz.tis.gcal.GCalService"%>
<%@page import="it.bz.tis.gcal.GCalContext"%>
<%@page import="com.google.gdata.data.calendar.CalendarEntry"%>

<%
{	// start own name space
	GCalService service = GCalService.getInstance(GCalContext.getInstance(request));
	if (service != null) {
		CalendarFeed resultFeed = service.getCalendars();
		if (resultFeed != null) {
%>
			
<div class="well calendar">
<h1>Calendars</h1>
			<ul>
<%
			for (CalendarEntry entry : resultFeed.getEntries() ) {
			    if (entry != null) {
%>
					<li>
					<%=entry.getTitle().getPlainText() %>
					</li>
<%
			    }
			}
%>
			</ul>
			</div>
<%
		}
	}

}
%>