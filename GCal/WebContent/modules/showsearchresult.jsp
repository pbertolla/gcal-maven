
<%@page import="java.util.Collections"%>
<%@page import="util.CalendarEntryComparator"%>
<%@page import="java.util.List"%>
<%@page import="util.Exporter"%>
<%@page import="java.util.Date"%>
<%@page import="com.google.gdata.data.Person"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.google.gdata.data.calendar.CalendarFeed"%>
<%@page import="it.bz.tis.gcal.GCalService"%>
<%@page import="it.bz.tis.gcal.GCalContext"%>
<%@page import="com.google.gdata.data.calendar.CalendarEventFeed"%>
<%@page import="com.google.gdata.data.calendar.CalendarEventEntry"%>
<div class="container well">

<%
{	// start own name space
	GCalService service = GCalService.getInstance(GCalContext.getInstance(request));
	String worksheet = GCalContext.getInstance(request).getWorksheet();
	List<String> worksheets=GCalContext.getInstance(request).getSsNames();
	List<String> backlogs=GCalContext.getInstance(request).getBacklogs();
	if (service != null) {
		CalendarEventFeed resultFeed = service.getCalendarEvents(GCalContext.getInstance(request).getSearchString());
		if (resultFeed != null) {
%>


	<div>
		<div class="container">
			<h1 class="pull-left">Search Results</h1>
			<a class="btn pull-right" href="csvexport">Export as ods</a>
			<form action="googlexport" method="get">
				<input type="submit" class="btn pull-right" value="Export to google Docs"/>
				<select name="log" class="pull-right">
				<% for (String sheet:backlogs){
				//String selected=sheet.equals(worksheet)?"selected='selected'":"";
				%>
				<option value="<%=sheet%>"><%=sheet%></option>
				<%}
				%>
				</select>
				<select name="worksheet" class="pull-right">
				<% for (String sheet:worksheets){
				String selected=sheet.equals(worksheet)?"selected='selected'":"";
				%>
				<option value="<%=sheet%>" <%=selected %>><%=sheet%></option>
				<%}
				%>
				</select>
				<%-- <input type="text" class="pull-right"
							<%if (worksheet != null) { %> 
	      		value="<%=worksheet %>"
			<%} %>
				/>		 --%>
			</form>
		</div>
		<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>Authors</th>
				<th>Startdate</th>
				<th>Enddate</th>
			</tr>
		</thead>
		<tbody>
			<%
			List<CalendarEventEntry> entries = resultFeed.getEntries();
			Collections.sort(entries,Collections.reverseOrder(new CalendarEntryComparator()));
			for (CalendarEventEntry entry : entries ) {
			    if (entry != null) {
			%>
			<tr>
				<td><%=entry.getTitle().getPlainText()%></td>
				<td><%=entry.getPlainTextContent()%></td>
				<td>
					<%	StringBuffer string = new StringBuffer("");
							for (Person person:entry.getAuthors()){
								if (string.length()!=0)
									string.append(", ");
								string.append(person.getName());
							}
							%> <%=string.toString()%>
				</td>
				<td>
				<% 
				   String startDate,endDate;
				   if(entry.getTimes().get(0).getStartTime().isDateOnly()){
					   startDate=Exporter.dayformatter.format(entry.getTimes().get(0).getStartTime().getValue());
					   endDate=Exporter.dayformatter.format(entry.getTimes().get(0).getEndTime().getValue());
				   }
				   else{
					   DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					   startDate=format.format(entry.getTimes().get(0).getStartTime().getValue());
					   endDate=format.format(entry.getTimes().get(0).getEndTime().getValue());
				   }
				%>
				<%=startDate%>
				</td>
				<td><%=endDate%></td>
				</tr>
			<%
			    }
			}
%>
		</tbody>

		</table>
	</div>
	<%
		}
	}

}
%>
</div>