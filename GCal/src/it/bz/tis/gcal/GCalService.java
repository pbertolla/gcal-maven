/**
 * GCal
 * Copyright (C) 2011 Patrick Ohnewein
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package it.bz.tis.gcal;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import com.google.gdata.client.Query;
import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.calendar.CalendarEventFeed;
import com.google.gdata.data.calendar.CalendarFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class GCalService {
	
	private static HashMap<GCalContext, GCalService> instanceMap;
	
	private GCalContext context;
	private CalendarService googleService;
	
	private GCalService(GCalContext context) {
		super();
		this.context = context;
	}
	
	public static GCalService getInstance(GCalContext context) {
		GCalService service = null;
		if (context != null) {
			if (instanceMap != null) {
				service = instanceMap.get(context);
			}
			if (service == null) {
				service = new GCalService(context);
				if (instanceMap == null) {
					instanceMap = new HashMap<GCalContext, GCalService>();
					instanceMap.put(context, service);
				}
			}
		}
		return service;
	}
	
	private CalendarService getGoogleService() {
		try {
			String username = context.getUsername();
			if (username != null) {
				googleService = new CalendarService("exampleCo-exampleApp-1.0");
				googleService.setUserCredentials(username, context.getPassword());
			}
		} catch (AuthenticationException e) {
			googleService = null;
			e.printStackTrace();
		}
		return googleService;
	}

	public CalendarFeed getCalendars() {
		CalendarFeed resultFeed = null;
		CalendarService googleService = getGoogleService();
		if (googleService != null) {
			URL feedUrl;
			try {
				feedUrl = new URL("http://www.google.com/calendar/feeds/default/allcalendars/full");
				resultFeed = googleService.getFeed(feedUrl, CalendarFeed.class);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		return resultFeed;
	}

	public CalendarEventFeed getCalendarEvents(String searchString) {
		CalendarEventFeed myResultsFeed = null;
		CalendarService googleService = getGoogleService();
		if (googleService != null) {
			try {
				//Create a new query object and set the parameters
				URL feedUrl = new URL("https://www.google.com/calendar/feeds/default/private/full");
				Query googleQuery = new Query(feedUrl);
				googleQuery.setFullTextQuery(searchString);

				//Send the request with the built query URL
				myResultsFeed = googleService.query(googleQuery, CalendarEventFeed.class);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ServiceException e) {
				e.printStackTrace();
			}			
		}
		return myResultsFeed;
	}
}
