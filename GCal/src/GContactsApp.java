/**
 * GCal
 * Copyright (C) 2011 Patrick Ohnewein
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gdata.client.Query;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.data.Content;
import com.google.gdata.data.TextContent;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.extensions.Email;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;


/**
 * Some time ago I installed a plugin for Thunderbird, which syncs the
 * contacts directly to Google. The problem is, that it creates a new
 * contact on Google, but it doesn't set the E-Mail-Address field. Instead
 * it just inserts the e-mail address as part of the Contact's Memo field.
 * 
 * Let's say I have a Contact in my ThunderBird with the following fields:
 * 
 * Name: Patrick Ohnewein
 * E-Mail: patrick.ohnewein@tis.bz.it
 * 
 * The result would be, that I get a Google Contact with the following
 * attributes:
 * 
 * Name: Patrick Ohnewein
 * Note: Anzeigename: Patrick Ohnewein
 *       Primäre E-Mail: patrick.ohnewein@tis.bz.it
 * 
 * No e-mail address field set  :-(
 * 
 * You can see, the Note Attribute is a multiline field. Sometimes it
 * contains even more information, but the pattern I observed is, the
 * "Primäre E-Mail: ..." is always on a single line.
 *  
 * So I wrote the Java code you can find at the bottom of this e-mail,
 * using Google Java Client Library, to find the Google contacts which do
 * not have any e-mail address set and which contain the "Primäre E-Mail: "
 * in the Notes. Found an entry with this pattern, the app transfers the
 * E-Mail from the notes to the contact e-mail field.
 *  
 * An additional function of the program: The first version of the app had
 * an error, it didn't set the e-mail field to the e-mail address, but to
 * "Primäre E-Mail: email address". So I added a recognition of this
 * pattern and error correction. This should not be needed for whom never
 * used the app version with the error, but it has been written to be save
 * to be left in the code.
 *  
 * @author Patrick Ohnewein
 */
public class GContactsApp {

	public static void main(String[] args) {
		String userName = args != null && args.length > 0 ? args[0] : null;
		String password = args != null && args.length > 1 ? args[1] : null;
		if (userName != null && password != null) {
			try {
				ContactsService service = new ContactsService("Google-contactsExampleApp-3");
				service.setUserCredentials(userName, password);

				URL feedUrl = new URL("http://www.google.com/m8/feeds/contacts/default/thin");

				Query myQuery = new Query(feedUrl);
				myQuery.setMaxResults(10000);
				ContactFeed resultFeed = service.query(myQuery, ContactFeed.class);

				System.out.println(resultFeed.getTitle().getPlainText());
				int entryCount = 0;
				for (ContactEntry entry : resultFeed.getEntries()) {
					entryCount++;
					ContactEntry contact = entry;
					int eMailCount = 0;
					boolean eMailChanged = false;
					for (Email email : contact.getEmailAddresses()) {
						eMailCount++;

						// check for a specific suffix in the e-mail-addresses and remove it if present
						String emailTxt = email.getAddress();
						String suffix = "Primäre E-Mail: ";
						if (emailTxt != null && emailTxt.startsWith(suffix)) {
							email.setAddress(emailTxt.substring(suffix.length()));
							eMailChanged = true;
						}
					}
					if (eMailCount == 0) {
						Content content = contact.getContent();
						if (content instanceof TextContent) {
							TextContent tContent = contact.getTextContent();
							if (tContent != null) {
								String plainTextContent = tContent.getContent().getPlainText();
								int idxPrimaryAddress = -1;
								String suffix = "Primäre E-Mail: ";
								if (plainTextContent != null && (idxPrimaryAddress = plainTextContent.indexOf(suffix)) >= 0) {
									// this contact has no e-mail address but the Primäre E-Mail in the notes field.
									// We transfer the content of Primäre E-Mail into teh contact as a e-mail field.
									System.out.println("\nContact has no e-mail addresses!");
									System.out.println("Id: " + contact.getId()); 
									if (contact.getTitle() != null) {
										System.out.println("Contact name: " + contact.getTitle().getPlainText());
									} else {
										System.out.println("Contact has no name");
									}
									System.out.println("TextContent: " + plainTextContent);

									String primaereEMail = plainTextContent.substring(idxPrimaryAddress + suffix.length());
									int idxNewLine = primaereEMail.indexOf('\r');
									if (idxNewLine < 0)
										idxNewLine = primaereEMail.indexOf('\n');
									if (idxNewLine >= 0) {
										primaereEMail = primaereEMail.substring(0, idxNewLine);
									}
									System.out.println("SET NEW EMAIL TO " + primaereEMail);
									Email email = new Email();
									email.setLabel("Work");
									email.setAddress(primaereEMail);
									contact.addEmailAddress(email);

									URL editUrl = new URL(contact.getEditLink().getHref());
									contact = service.update(editUrl, contact);
								}
							}
						}
					}
					else if (eMailChanged) {
						System.out.println("\nChange E-Mail");
						if (contact.getTitle() != null) {
							System.out.println("Contact name: " + contact.getTitle().getPlainText());
						} else {
							System.out.println("Contact has no name");
						}					
						URL editUrl = new URL(contact.getEditLink().getHref());
						contact = service.update(editUrl, contact);
					}
				}
				System.out.println("Computed " + entryCount + " contacts!");
			} catch (AuthenticationException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ServiceException e) {
				e.printStackTrace();
			}
		}
		else {
			System.out.println("usage: GContactsApp <gMail-Address> <password>");
		}
	}
}
