/*
 * GCal
 * Copyright (C) 2011 Patrick Ohnewein
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WAllerheiligenITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package it.bz.tis.gcal.actions;

import it.bz.tis.gcal.GCalContext;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.SpreadsheetgdocsService;
import util.Exporter;

import com.google.gdata.data.DateTime;

/**
 * Servlet implementation class SetParameters
 */
public class SetParameters extends HttpServlet {

	private static final long serialVersionUID = 1L;
        private static final String GMAIL_SUFFIX="@gmail.com";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SetParameters() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		GCalContext context = GCalContext.getInstance(request);
		String username=request.getParameter("username");
		if(!username.endsWith(GMAIL_SUFFIX))
			username+=GMAIL_SUFFIX;
		context.setUsername(username);
		context.setPassword(request.getParameter("password"));
		context.setSearchString(request.getParameter("searchstring"));
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		SpreadsheetgdocsService service = SpreadsheetgdocsService.getInstance();
		service.authenticate(request.getParameter("username"),request.getParameter("password"));
		Date sDate=null,eDate=null;
		try {
			sDate = Exporter.dayformatter.parse(request.getParameter("startdate"));
			eDate = Exporter.dayformatter.parse(request.getParameter("enddate"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		context.setStartDate(new DateTime(sDate));
		context.setEndDate(new DateTime(eDate));
		context.setSsNames(service.getSpreadSheetNames());
		context.setSpreadSheetName("Backlog 2012");
		context.setWorksheet("Patrick Bertolla");
		context.setBacklogs(service.getWorksheetNamesBySpreadheetName("Backlog 2012"));
		String jspPage = "index.jsp";	
		RequestDispatcher view = request.getRequestDispatcher(jspPage);
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
