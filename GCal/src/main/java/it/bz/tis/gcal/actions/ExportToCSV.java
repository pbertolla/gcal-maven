/*
 * GCal
 * Copyright (C) 2011 Patrick Ohnewein
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WAllerheiligenITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package it.bz.tis.gcal.actions;

import it.bz.tis.gcal.GCalContext;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.CSVExporter;

/**
 * Servlet implementation class SetParameters
 */
public class ExportToCSV extends HttpServlet {

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ExportToCSV() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		GCalContext context = GCalContext.getInstance(request);
		CSVExporter exporter = new CSVExporter();
		exporter.exportData(context.getMyResultsFeed());
		
		String jspPage = "index.jsp";	
		RequestDispatcher view = request.getRequestDispatcher(jspPage);
		view.forward(request, response);
	}

}
