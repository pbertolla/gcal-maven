package service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

public class SpreadsheetgdocsService {

	private static SpreadsheetService service;
	private static SpreadsheetgdocsService myService;
	private static URL SPREADSHEET_FEED_URL;
	public SpreadsheetgdocsService() {
		if (service==null){
			service = new SpreadsheetService("MySpreadsheetIntegration-v1");
			try {
				SPREADSHEET_FEED_URL = new URL(
						"https://spreadsheets.google.com/feeds/spreadsheets/private/full");
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
	}

	public void authenticate(String username, String password) {
		try {
			service.setUserCredentials(username,password);
		} catch (AuthenticationException e) {
			e.printStackTrace();
		}

		
	}

	public static SpreadsheetgdocsService getInstance() {
		if (myService==null)
			myService = new SpreadsheetgdocsService();
		return myService;
	}

	public List<String> getWorksheetNamesBySpreadheetName(String name) {
		List<WorksheetEntry> workSheets= new ArrayList<WorksheetEntry>();
		try {
			List<SpreadsheetEntry> spreadsheets = getSpreadsheets();
			if (!spreadsheets.isEmpty()) {
				SpreadsheetEntry sheet= getSpreadSheetByName(name,spreadsheets);
				workSheets=sheet.getWorksheets();
			}
			
		} catch (ServiceException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return getNames(workSheets);
	}

	public SpreadsheetEntry getSpreadSheetByName(String name,List<SpreadsheetEntry> spreadsheets) {
		SpreadsheetEntry spreadSheet=null;
		for (SpreadsheetEntry entry : spreadsheets) {
			if (entry.getTitle().getPlainText().equals(name)) {
				spreadSheet = entry;
				break;
			}
		}
		return spreadSheet;
	}

	public static List<SpreadsheetEntry> getSpreadsheets() {
		SpreadsheetFeed ssfeed;
		List<SpreadsheetEntry> spreadsheets= new ArrayList<SpreadsheetEntry>();
		try {
			ssfeed = service.getFeed(SPREADSHEET_FEED_URL,
					SpreadsheetFeed.class);
			spreadsheets = ssfeed.getEntries();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return spreadsheets;
	}

	private List<String> getNames(List<WorksheetEntry> worksheets) {
		List<String> names= new ArrayList<String>();
		for(WorksheetEntry entry: worksheets){
			names.add(entry.getTitle().getPlainText());
		}
		return names;
	}
	public List<String> getSpreadSheetNames(){
		List<SpreadsheetEntry> spreadsheets=getSpreadsheets();
		List<String> names= new ArrayList<String>();
		for(SpreadsheetEntry entry : spreadsheets){
			String name = entry.getTitle().getPlainText();
			if (name.toLowerCase().contains("backlog"))
				names.add(name);
		}
		return names;
	}

	public ListEntry insertRow(URL listFeedUrl, ListEntry row) throws IOException, ServiceException {
		
		return service.insert(listFeedUrl, row);
	}
}
