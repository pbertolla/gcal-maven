package util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.google.gdata.data.calendar.CalendarEventFeed;

public interface Exporter {
	public static DateFormat dayformatter = new SimpleDateFormat("dd/MM/yyyy");
	public static DateFormat timeformatter = new SimpleDateFormat("HH:mm:ss");
	public void exportData(CalendarEventFeed feed);
}
