package util;

import it.bz.tis.gcal.GCalContext;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import service.SpreadsheetgdocsService;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.DateTime;
import com.google.gdata.data.calendar.CalendarEventEntry;
import com.google.gdata.data.calendar.CalendarEventFeed;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;

public class GoogleDocsExporter implements Exporter {

	private GCalContext context;
	private SpreadsheetgdocsService service;
	public GoogleDocsExporter() {
	}
	
	public GoogleDocsExporter(GCalContext context) {
		this.context=context;
	}

	@Override
	public void exportData(CalendarEventFeed feed)  {
		service = SpreadsheetgdocsService.getInstance();
		service.authenticate(context.getUsername(),context.getPassword());
		try {

			List<SpreadsheetEntry> spreadsheets = SpreadsheetgdocsService.getSpreadsheets();
			if(!spreadsheets.isEmpty()){
				SpreadsheetEntry spreadSheet=service.getSpreadSheetByName(context.getSpreadSheetName(),spreadsheets);
				if (spreadSheet!=null)
				{
					for (WorksheetEntry worksheet:spreadSheet.getWorksheets()){
						if( worksheet.getTitle().getPlainText().equals(context.getWorksheet())){
							URL listFeedUrl = worksheet.getListFeedUrl();
							Calendar c = Calendar.getInstance();
							List<CalendarEventEntry> entries = feed.getEntries();
							Collections.sort(entries,new CalendarEntryComparator() );
							for (CalendarEventEntry entry : entries) {
								DateTime googleDate = entry.getTimes().get(0).getStartTime();
								Date myDate = new Date(googleDate.getValue());
								Date endDateTime = new Date(entry.getTimes().get(0).getEndTime().getValue());
								c.setTime(myDate);
								int week_of_year = c.get(Calendar.WEEK_OF_YEAR);
								String starttime="", endtime="";
								if (!googleDate.isDateOnly()) {
									starttime = timeformatter.format(myDate);
									endtime = timeformatter.format(endDateTime);
									createRow(listFeedUrl, entry, myDate,week_of_year, starttime, endtime);
								}
								else{
									Set<Date> days=calculateWorkDays(myDate,endDateTime);
									for (Date date:days){
										createRow(listFeedUrl, entry, date, week_of_year, starttime, endtime);
									}
									
								}					
							}
						}
						
					}
				}
			}
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	private SortedSet<Date> calculateWorkDays(Date startDate, Date endDate) {
		Calendar startCal;
		Calendar endCal;
		startCal = Calendar.getInstance();
		startCal.setTime(startDate);
		endCal = Calendar.getInstance();
		endCal.setTime(endDate);
		SortedSet<Date> days= new TreeSet<Date>();
		while (startCal.getTimeInMillis() < endCal.getTimeInMillis()){
			if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
					&& startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				days.add(startCal.getTime());
			}
			startCal.add(Calendar.DATE, 1);
		}

		return days;
	}

	private void createRow(URL listFeedUrl, CalendarEventEntry entry,
			Date myDate, int week_of_year, String starttime, String endtime)
			throws IOException, ServiceException {
		ListEntry row = new ListEntry();
		String description = composeDescription(entry); 
		row.getCustomElements().setValueLocal("Week", String.valueOf(week_of_year));
		row.getCustomElements().setValueLocal("Date",dayformatter.format(myDate));
		row.getCustomElements().setValueLocal("Project", entry.getTitle().getPlainText());
		row.getCustomElements().setValueLocal("Activitydescription", description);
		row.getCustomElements().setValueLocal("StartTime", starttime);
		row.getCustomElements().setValueLocal("EndTime", endtime);							
		row = service.insertRow(listFeedUrl, row);
	}
	
	private String composeDescription(CalendarEventEntry entry){
		StringBuffer description = new StringBuffer("");
		String title= entry.getTitle().getPlainText();
		if (title.contains(":")){
			String[] descParts= title.split(":");
			if(descParts.length>1)
				description.append(descParts[1]);
		}
		description.append(entry.getPlainTextContent());
		return description.toString();
	}

	

}
