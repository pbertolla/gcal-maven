package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import au.com.bytecode.opencsv.CSVWriter;

import com.google.gdata.data.DateTime;
import com.google.gdata.data.calendar.CalendarEventEntry;
import com.google.gdata.data.calendar.CalendarEventFeed;

public class CSVExporter implements Exporter {

	private static final String FILENAME = "goolgedata";
	private static final String FILETYPE = ".ods";

	public CSVExporter() {
		// TODO Auto-generated constructor stub
	}
	
	public void exportData(CalendarEventFeed feed){
		File f = new File(FILENAME + timeformatter.format(new Date())+FILETYPE);
		CSVWriter csvw=null;
		try {
			csvw = new CSVWriter(new FileWriter(f));
			for (CalendarEventEntry entry : feed.getEntries()) {
				String[] line = new String[6];
				Calendar c = Calendar.getInstance();
				DateTime googleDate = entry.getTimes().get(0).getStartTime();
				Date myDate = new Date(googleDate.getValue());
				Date endTime = new Date(entry.getTimes().get(0).getEndTime().getValue());
				c.setTime(myDate);

				int week_of_year = c.get(Calendar.WEEK_OF_YEAR);
				line[0] = String.valueOf(week_of_year);
				line[1] = dayformatter.format(myDate);
				line[2] = entry.getTitle().getPlainText();
				line[3] = entry.getPlainTextContent();
				line[4] = timeformatter.format(myDate);
				line[5] = timeformatter.format(endTime);
				csvw.writeNext(line);
			}
			csvw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				csvw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
