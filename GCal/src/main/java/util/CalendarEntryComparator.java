package util;

import java.util.Comparator;

import com.google.gdata.data.calendar.CalendarEventEntry;

public class CalendarEntryComparator implements Comparator<CalendarEventEntry> {

	@Override
	public int compare(CalendarEventEntry o1, CalendarEventEntry o2) {
		return o1.getTimes().get(0).getStartTime().compareTo(o2.getTimes().get(0).getStartTime());
	}

}
